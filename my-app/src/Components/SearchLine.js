import React, {Component} from "react";
import styled from "styled-components";
import {Button, TextInput} from "./Controls/Controls";

class SearchLine extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    };

    getVacanciesButtonOnClick = () => {
        window.vacsService.sendRequest(this.state.text);
    };

    onTextChange = (event) => {
        this.setState({text: event.target.value});
    };

    onKeyUp = (event) => {
        if(event.key === "Enter")
        {
            this.getVacanciesButtonOnClick();
        }
    };

    render() {
        return (
            <Wrapper className={this.props.className}>
                <TextInput value={this.state.text} onChange={this.onTextChange} onKeyUp={this.onKeyUp} placeholder="Я ищу..."/>
                <Button onClick={this.getVacanciesButtonOnClick}>
                    Найти
                </Button>
            </Wrapper>

        );
    }
}

//#region Styled

const Wrapper = styled.div`
  display: flex;
  ${TextInput}{
  flex: auto;
  }
`;

//#endregion

export default SearchLine;