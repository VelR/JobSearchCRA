import React, {Component} from 'react';
import {Button} from "./Controls/Controls";
import styled from "styled-components";
import TextField from "./Controls/TextField";

class Sidebar extends Component {

    onSalaryChange = (event) => {
        window.vacsService.salary = event.target.value;
    };

    render() {
        return (
            <Wrapper>
                <h3>Настройка поиска</h3>
                <StyledTextField title={'Зарплата'} onChange={this.onSalaryChange} type="text"/>
            </Wrapper>
        )
    }
}

//#region Styled SideBar

const StyledTextField = styled(TextField)``;

const Wrapper = styled.aside`
  display: flex;
  flex-direction: column;
  align-content: baseline;
  h3{
  text-align: center;
  margin: 0.5rem;
  }
  ${Button},${StyledTextField} {
  margin: 0.5rem 0;
  }
`;

//#endregion

export default Sidebar;