import React, {Component} from 'react';
import styled from 'styled-components'
import {TextInput} from "./Controls";

class TextField extends Component {
    render() {
        if (this.props.title == null) {
            return (
                <Wrapper className={this.props.className}>
                    <TextInput onChange={this.props.onChange}/>
                </Wrapper>
            )
        }
        else {
            return (
                <Wrapper className={this.props.className}>
                    <Title>
                        {this.props.title}
                    </Title>
                    <TextInput onChange={this.props.onChange}/>
                </Wrapper>
            )
        }

    }
}

export default TextField;

//#region Styled

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  padding: 0 0 0.2rem 0.5rem;
`;

//#endregion



