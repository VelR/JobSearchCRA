import React, { Component} from 'react';
import styled from "styled-components";

class VacanciesItem extends Component {
    render() {
        let item = this.props.item;
        const vacsService = window.vacsService;
        return(
            <Wrapper className={this.props.className}>
                <Header>
                    <div className="title">
                        {item.name}
                    </div>
                    <div className="salary">
                        {vacsService.getSalary(item)}
                    </div>
                </Header>

                <div>
                    <p>{item.snippet.responsibility}</p>
                    <p>{item.snippet.requirement}</p>
                </div>

                <Footer>
                    <div className="address">
                        {vacsService.getAddres(item)}
                    </div>

                    <div className="date">
                        {vacsService.getDate(item)}
                    </div>
                </Footer>
            </Wrapper>
        )
    }
}

//#region Styled

const Wrapper = styled.div`
    background: ${props => props.theme.contentBackground};    
    color: ${props => props.theme.font};
    padding: 0.5rem;
    text-decoration: none;
    display: flex;
    flex-direction: column;
    border: 1px solid ${props => props.theme.border};
    cursor: pointer;
    margin: 0 0 1rem 0;
    &:hover{
        background-color: ${props => props.theme.contentBackgroundHover};
        color: ${props => props.theme.font};
        box-shadow: ${props => props.theme.shadowBlack};
        border: 1px solid ${props => props.theme.borderHover};
    }  
`;

const Header = styled.div`
    display: flex;
    margin: 1rem 0;
    align-content: space-between;
    *{
    font-size: 18px;
    }
    .title{
    flex: 2;
    flex-wrap: wrap;
    font-weight: bold;
    }
    .salary{
    flex: 1;
    margin: auto;
    text-align: right;
}
`;

const Footer = styled.div`
    display: flex;
    margin: 1rem 0;
    align-content: space-between;
    *{
    color: ${props => props.theme.fontMeta};
    font-size: 14px;
    }
    .address{
    flex: 1;
    }
`;

//#endregion

export default VacanciesItem;

