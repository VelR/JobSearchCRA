import styled from "styled-components";

export const Button = styled.button`
    background: ${props => props.theme.buttonBackground};
    color: ${props => props.theme.buttonFont};
    padding: 0.5rem 1rem;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    border: 1px solid ${props => props.theme.border};
    cursor: pointer;
    &:hover{
      background-color: ${props => props.theme.borderHover};
      color: ${props => props.theme.font};
      box-shadow: ${props => props.theme.shadowBlack};
      border: 1px solid ${props => props.theme.borderHover};
    }  
`;

export const TextInput = styled.input.attrs({
    type: 'text'
})`
    border: 1px solid ${props => props.theme.border};
    padding: 0.5rem;
    background-color: ${props => props.theme.contentBackground};
    color: ${props => props.theme.font};
        &:hover{
        border: 1px solid ${props => props.theme.borderHover};
        }
`;

