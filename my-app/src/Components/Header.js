import React, {Component} from 'react';
import {Page} from "../Containers/Page";
import styled from  'styled-components'

class Header extends Component {

    render() {
        return (
            <HeaderWrapper>
                <Page>
                    <Title>
                        Search vacancies application from Vel.
                    </Title>
                </Page>
            </HeaderWrapper>
        )
    }
}

//#region Styled

const HeaderWrapper = styled.div`
  background: #202020;
  color: #fff;
`;

const Title = styled.header`
  padding: 2rem;
  font-size: 2rem;
  text-align: center;
  margin: auto;
`;

//#endregion

export default Header;