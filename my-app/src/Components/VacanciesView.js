import React, {Component} from 'react';
import VacancyItem from "./Controls/VacanciesItem";
import styled from "styled-components";

class VacanciesView extends Component {
    render() {
        if(this.props.itemSource != null)
        {
            return (
                <div>
                    {this.props.itemSource.map((item, i) => <VacancyItem key={i} item={item}/>)}
                </div>
            )
        }
        else {
            return (
                <NotFound>
                    По вашему запросу ничего не найдено
                </NotFound>
            )
        }
    }
}

//#region Styled.

const NotFound = styled.div`
  text-align: center;
  margin: 1rem;
`;

//#endregion


export default VacanciesView;
