import React, {Component} from 'react';
import {Page} from "../Containers/Page";
import styled from  'styled-components'
import Sidebar from './Sidebar';
import VacanciesView from "./VacanciesView";
import SearchLine from "./SearchLine";

class SearchView extends Component {
    constructor(props){
        super(props);
        this.state = {
            vacancies: []
        };
    };

    // Слушает список вакансий.
    componentDidMount(){
        window.addEventListener('vacanciesChanged', this.onVacanciesChanged)
    };

    // Регаирует на изменение вакансий.
    onVacanciesChanged = () =>{
        this.setState({vacancies: window.vacsService.vacancies})
    };

    render() {
        return (
            <Page>
                <MainWrapper>
                    <Sidebar/>
                    <StackWrapper>
                        <StyledSearchLine/>
                        <VacanciesView itemSource={this.state.vacancies}/>
                    </StackWrapper>
                </MainWrapper>
            </Page>
        )
    }
}

//#region Styled

const StyledSearchLine = styled(SearchLine)`
  padding: 0 0 1rem 0;
`;

const MainWrapper = styled.div`
  display: flex;
  padding: 1rem 0;  
`;

const StackWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 0 0 1rem;
  flex: auto;
  
`;

//#endregion

export default SearchView;