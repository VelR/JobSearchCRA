import styled from "styled-components";

export const Page = styled.div`
width: 60%;
margin: auto;
@media (max-width: 1200px) {
      width: 95%;
    } 
`;

