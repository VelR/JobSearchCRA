import moment from "moment";

;(function () {
    //#region Переменные.

    let vacancies = null;

    //#endregion

    //region Конструктор.

    function vacanciesService() {
    }

    //endregion Конструктор.

    //#region Публичные Методы и свойства.

    // Отправить запрос по URL или default.
    vacanciesService.sendRequest = sendRequest;
    vacanciesService.getSalary = getSalary;
    vacanciesService.getAddres = getAddress;
    vacanciesService.getDate = getDate;

    // Вакансии
    vacanciesService.vacancies = vacancies;

    // Экспорт наружу.
    // Хочу не к window а в переменную
    window.vacsService = vacanciesService;

    //#endregion

    //#region События.

    // Изменение вакансий.
    let vacanciesChanged = new Event('vacanciesChanged', {bubbles: true});

    //#endregion

    //#region Методы

    // Отправить Get запрос по URL и сохранить результат в vacancies.
    function sendRequest(text = null) {
        let URL = 'https://api.hh.ru/vacancies?';

        if (!isNullOrWhiteSpace(text)) {
            URL += `text=${text}&&`
        }

        if (!isNullOrWhiteSpace(this.salary)) {
            URL += `salary=${this.salary}&&only_with_salary=true&&`
        }

        console.log(URL);

        (async () => {
            let response = await fetch(URL);
            if(response.status === 200)
            {
                let json = await response.json();
                this.vacancies = json.items;
                dispatchEvent(vacanciesChanged);
            }
            else{
                dispatchEvent(vacanciesChanged);
            }
        })();
    }

    function getSalary(v) {
        if (v.salary == null) return 'Зарплата по договоренности';
        const to = v.salary.to;
        const from = v.salary.from;
        const currency = v.salary.currency;
        let result = "";
        if (from) {
            result = `от ${from} `;
        }
        if (to) {
            result += `до ${to} `;
        }
        if (currency === "RUR") {
            result += `руб.`;
        }
        else if (currency === "USD") {
            result += '$';
        }
        else if (currency === "EUR") {
            result += '€';
        }
        else {
            result += currency;
        }

        return result;
    }

    function getAddress(v) {
        if (v.address == null || v.address.city == null) return 'Адрес не указан';
        let address = [v.address.city];
        const street = v.address.street;
        if (street) {
            address.push(street);
            const building = v.address.building;
            if (building) {
                address.push(building);
            }
        }
        return address.join(', ');
    }

    function getDate(v) {
        return moment(v.published_at).format("DD.MM.YYYY в HH:mm");
    }

    function isNullOrWhiteSpace(input) {
        return (!input || 0 === input.length);
    }

    //#endregion
}());