import React, {Component} from 'react';
import {ThemeProvider} from 'styled-components';
import Header from "./Components/Header";
import SearchView from "./Components/SearchView";
import styled from "styled-components"

import './Services/VacanciesServices'

const theme = {
    border: '#5e5e63',
    borderHover: '#4590ce',
    font: '#fff',
    fontMeta: '#999',
    background: '#2d2d2f',

    contentBackground: '#454549',
    contentBackgroundHover: '#525256',
    contentBorderHover: '#525256',

    buttonBackground: '#202020',
    buttonFont: '#bbb',

    shadowBlack: '0.25rem 0.25rem 0.5rem rgba(0, 0, 0, 0.7)',
    shadowWhite: '0.25rem 0.25rem 0.5rem rgba(255, 255, 255, 0.7)',
};

class App extends Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <div>
                    <Header/>
                    <SearchView/>
                </div>
            </ThemeProvider>
        );
    }
}

export default App;
